/* global */
var L = require('leaflet') // Initialize leaflet.js
var $ = require('jquery')
// var PathFinder = require('geojson-path-finder')
// var substringMatcher = require('./modules/form')
var PlanetsAutocomplete = require('./modules/_form')
var Router = require('./modules/_hyperspace-router')
// var explode = require('@turf/explode')
// var nearest = require('@turf/nearest')
// var helpers = require('@turf/helpers')
// var meta = require('@turf/meta')
// var extent = require('turf-extent')
// var lineDistance = require('@turf/line-distance')
// var lineString = helpers.lineString
// var point = helpers.point
// var multiPoint = helpers.multiPoint
// var coordEach = meta.coordEach
// var featureEach = meta.featureEach
// var featureCollection = helpers.featureCollection

var planets
var hyperspace_singlepart
$.when(
  $.get('data/hyperspace_singlepart.json', function (response) {
    hyperspace_singlepart = response
  }),
  $.get('data/planets.json', function (response) {
    planets = response
    PlanetsAutocomplete(planets)
  })

).then(function () {
  console.log('ooo')
  initialize(hyperspace_singlepart)
})


var map = L.map('map')
console.log('hi')
var style = {
  fillColor: 'blue',
  color: '#000',
  weight: 1,
  opacity: 1,
  fillOpacity: 0.7
}
console.log('hi3')

// initialize(hyperspace_singlepart)
// Router
function initialize (network) {
  console.log('INITIALIZING NETWORK: ' + network.name)
  //
  // var lothal = planets.features[2051]
  // var syMyrth = planets.features[946]
  // var centares = planets.features[1827]
  // var ossus = planets.features[942]
  // var pasmin = planets.features[796]
  // var dellalt = planets.features[790]
  // var monCalamari = planets.features[876]
  // var mantan = planets.features[878]
  // var sanctuary = planets.features[887]
  // var kamdon = planets.features[885]
  // var kashyyyk = planets.features[1902]
  //
  // var planetsID = [942, 2051, 946, 1827, 796, 790, 876, 878, 887, 885, 1902]
  // var i
  // for (i = 0; i < planetsID.length; i++) {
  //   addPlanetToMap(planetsID[i])
  // }
  //
  // function addPlanetToMap (planetID) {
  //   var planetFeature = planets.features[planetID]
  //   var planetLon = planetFeature.geometry.coordinates[0]
  //   var planetLat = planetFeature.geometry.coordinates[1]
  //   L.marker([planetLat, planetLon]).addTo(map).bindPopup(planetFeature.properties.name)
  // }

  var hyperspace = new L.GeoJSON(network, {
    style: style
  }).addTo(map)
  map.fitBounds(hyperspace.getBounds())

  var pathfinder = Router.createPathFinder(network, map)

  // add planets layer
  function getRadius (zoom) {
    console.log(zoom)
    return zoom === 0 ? 4
      : zoom === 1 ? 3
        : zoom === 2 ? 2
          : 1
  }
  function planetsStyle (feature) {
    return {
      radius: getRadius(feature.properties.zm),
      fillColor: '#ff7800',
      color: '#000',
      weight: 1,
      opacity: 1,
      fillOpacity: 0.8
    }
  }
  function onEachFeature (feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties.name) {
      layer.bindPopup(feature.properties.name)
    }
  }

  var planetLayer = L.geoJSON(planets, {
    style: planetsStyle,
    pointToLayer: function (feature, latlng) {
      return L.circleMarker(latlng, planetsStyle)
    },
    onEachFeature: onEachFeature,
    filter: function (feature, layer) {
      if (feature.properties.canon === 1) {
        return feature.properties.canon
      }
    }
  }).addTo(map)
  var overlayLayers = {
    Planets: planetLayer
  }
  L.control.layers(null, overlayLayers).addTo(map)
}
